# Spielwelt

## Albion

Albion ist das Land in dem wir uns aktuell befinden. Bisher ist noch nicht
allzuviel über Albion bekannt, es ist ein Land in dem Menschen leben und in dem
es Wälder, Wiesen und Gebirge gibt. In Albion gibt es (abgesehen von Elana)
keine Druiden.

## Dorf Kornwald

Das Dorf Kornwald liegt in Albion, es liegt inmitten von Wiesen, Feldern und
allgemein Grasland. In der Nähe gibt es ein mittelgroßes Gebirge - "die
Schuppen". Der Fuß der Berge liegt in etwa eine Tagesreise vom Dorf entfernt.
Kornwald liegt an einer Haupthandelsstraße und ist ein beliebter Umschlagplatz
für Händler jeder Art. Dadurch ist Kornwald auch reicher als andere Dörfer, der
Handel hat es reich gemacht. Somit ist es nicht verwunderlich, dass sich die
Bürger von Kornwald anders als in anderen Dörfern den Luxus einer kompletten
Holzpalisade rund um das ganze Dorf leisteten.
