# Aidan

## Allgemeine Beschreibung und Spielzüge zu Beginn

Aidan ist ein ziemlich junger Magier, ein wenig ungestühm und definitiv ein
wenig chaotisch.

Er hat durchdringende grau-blaue Augen und wirres braun-oranges Haar. Sein recht
schmächtiger Körper wirkt eher unsportlich und die seltsamen Roben die er des
öfteren trägt verstärken den leicht seltsamen Anblick zudem.

Er ist ein von Herzen guter Mensch, hat aber eine teilweise nicht ganze einfache
Vergangenheit erlebt, an die er sich zum aktuellen Zeitpunkt aber nicht
vollständig erinnern kann. Seine Eltern waren königlicher Abstammung in einem
alten Reich noch älterer Magier-Familien.

Allerdings weiß man hier aktuell nicht viel mehr, seine Eltern sind entweder
verschollen oder sogar tot, nur ein Kontakt aus der Vergangenheit ist ihm
geblieben: die Vertraute und Freundin seiner Eltern Elana.

Aidan ist bewusst dass heutzutage und vor allem in diesen Landen ein Magier
etwas besonderes ist und er versucht seine Fähigkeiten weiter auszubauen, sein
Hauptantrieb ist der Wunsch sich zu verbessern und zu lernen. Es geht doch
nichts über einen uberlasermegamagiemosh!

Alter: Aidan ist ca. 19 Jahre alt.

Um mehr über Aidans Vergangenheit herauszufinden, muss er erst mit der Gegenwart
klarkommen: zunächst muss er lernen, seine magischen Fähigkeiten sinnvoll
einzusetzen, sich selbst einschätzen lernen (Überschätzung..) und genug Kraft
sammeln sich der Vergangenheit zu stellen. Aktuell werden Erinnerungen teilweise
aus Angst überdeckt. Was allerdings klar ist: es gibt einen Onkel, und der ist
reich. Und mächtig. Und böse. (und früher war er vielleicht nicht so reich wie
heute...)

- Mensch -> Klerikerzauber "leichte Wunden heilen"
- GUT
- Attribute siehe unten, keine Modifikatoren (nicht Schwach, Zittrig o.ä.)
- Zauber wirken usw.

### Attribute

| Attribut         | Wert | Modifikator |
| -------:         | :--: | :---------- |
| Stärke           | 8    | -1          |
| Geschicklichkeit | 13   | +1          |
| Konstitution     | 9    | 0           |
| Intelligenz      | 16   | +2          |
| Weisheit         | 15   | +1          |
| Charisma         | 12   | 0           |

## Eigenschaften und Inventar

### TP

| Datum      | TP | Beschreibung                                                     |
| :-         | -: | :-                                                               |
| 04.05.2019 | 13 | Initialer Wert                                                   |
| 05.05.2019 | 8  | Kampf mit Wölfen ( 4 Stück, groß)                                |
| 05.05.2019 | 13 | wieder erholt am Lagerfeuer, Wolfsfleisch gegessen und ausgeruht |

### Stufe

| Datum      | Stufe | Beschreibung   |
| :-         | -:    | :-             |
| 04.05.2019 | 1     | Initialer Wert |


### EP
| Datum      | Wert | Beschreibung                                                                                                                                                                                               |
| -          | -    | -                                                                                                                                                                                                          |
| 04.05.2019 | 0    | Initialer Wert                                                                                                                                                                                             |
| 04.05.2019 | 4    | "Wölfe sind doof": bei Kampf mit Wölfen hab ich erstaunliches Würfelglück bewiesen und bin so richtig im Matsch gelandet. Die Waffe wurde mir abgenommen und ich lag am Boden unter einem Wolf begraben... |
| 04.05.2019 | 5    | Bande Alix                                                                                                                                                                                                 |
| 04.05.2019 | 6    | hab was für meine Gesinnung getan (GUT: Magie um zu helfen)                                                                                                                                                |
| 04.05.2019 | 7    | wir haben was über die Welt erfahren (vgl. Beschreibung Albion)                                                                                                                                            |
| 04.05.2019 | 8    | bedeutendes Monster oder Gegner erledigt/kennengelernt? Große Wölfe jedenfalls.. (??)                                                                                                                      |
| 04.05.2019 | 8    | Abschluss der Session                                                                                                                                                                                      |

### Gold
| Datum      | Wert | Beschreibung                                             |
| -          | -    | -                                                        |
| 04.05.2019 | 50   | Initialer Wert                                           |
| 04.05.2019 | 45   | in der Schenke was für Getränke und Übernachtung gezahlt |

### erlernte Zauber
| Datum      | Wert                  | Beschreibung                                                                                                 |
| -          | -                     | -                                                                                                            |
| 04.05.2019 | Zaubertricks          | Licht, Unsichtbarer Diener, Zaubertrick                                                                      |
| 04.05.2019 | leichte Wunden heilen | Berührung: 1W8 Heilung (Verschorftes Gewebe heilen u.ä.), erlernt durch Spielzug zu Beginn -> Volk -> Mensch |
| 04.05.2019 | Unsichtbarkeit        | 1. Grad, Illusion, Dauerhaft                                                                                 |
| 04.05.2019 | Magisches Geschoss    | 1. Grad, Hervorrufung                                                                                        |
| 04.05.2019 | Alarm                 | 1. Grad                                                                                                      |

### Spielzüge für Fortgeschrittene
| Datum      | Wert | Beschreibung |
| -          | -    | -            |
| 04.05.2019 |      | bisher keine |

### Gepäck
04.05.2019:

| Name         | Beschreibung | Gewicht | Anwendungen |
| ----         | ------------ | :-:     | :-:         |
| Lederrüstung | Rüstung 1    | 1       |             |
| Büchertasche |              | 1       | 5           |
| 3 Heiltränke |              | 0       |             |
| Dolch        | Hand         | 1       |             |
| 3 Gegengifte |              | 0       |             |

