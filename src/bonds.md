# Bande

## kleine Bande

### 04.05.2019, nicht erfüllt
Auch ohne Magie kann man mit Tricks und Tücke kämpfen: ich will Alix beweisen,
dass ich ohne meine Magie nicht komplett nutzlos bin! Ich werde ohne Magie einen
Goblin töten, dann wird sie schon sehen.

###  04.05.2019, nicht erfüllt
Magie ist so vielfältig, es ist nicht nur die reine Zerstörungskraft und Macht,
welche die Magie ausmacht. Auch viel schönes und hilfreiches kann man mit Magie
erreichen. Ich werde Elana und Alix zeigen, wie vielfältig Magie ist, indem ich
bei Gelegenheit einen Zaubertrick einsetzen werde. Vielleicht wird ja sogar der
"Zauber der Woche" ein regelmäßiges Ereignis, bei dem ich jede Woche meine
neuesten Errungenschaften vorstelle oder zumindest erzähle was ich gelesen habe.

## große Bande und Geschichten

### Alix (1/10)

"Alix ist bedauernswert ahnungslos, was die Welt betrifft. Ich werde ihr
beibringen, was ich kann."

Es geht hierbei darum, dass Alix als eher pragmatischer Barbar der Schönheit und
den magischen Feinheiten der Welt nichts abgewinnen kann. Alix hat sich
vorgenommen, ihr zu zeigen, dass es durchaus interessante, wichtige, hilfreiche
und mächtige Dinge gibt, die ein Barbar auf den ersten Blick verpasst.

1. Im Kampf gegen die großen Wölfe konnte ich zeigen, dass auch ein Magier
sinnvoll zu einem Kampf beitragen kann. Auch wenn Alix den größten Teil der
Wölfe im Alleingang bekämpft hat, schaute sie schon nicht schlecht, als ich den
letzten Wolf mit meinen magischen Geschossen regelrecht in seine Moleküle
aufgelöst habe! Und das, obwohl ich ihr nur knapp bis zu den Schultern gehe.

### Elana (offen)

Elana hat mir ein Ritual gezeigt... hat/wird.. warum? welches?

### Aidan (offen)

- Ich muss herausfinden, was mit meinen Eltern geschehen ist!
- Warum musste ich fliehen, was ist passiert?
- Was hat Elana früher gemacht?
- Und wie bin ich eigentlich zum Magier geworden?
- Und vor allem: was hat mein Onkel mit dem Verschwinden meiner Eltern zu tun?
