# Andere Personen und Charaktere in Albion

## Personen in Kornwald

### Hycorax Kornwald

Hycorax ist der Dorfälteste. Er hat hier ziemlich viel Einfluss und ist ziemlich
reich. Er lebt in der Mitte des Dorfes in einem deutlich größeren Haus als alles
andere in Kornwald. Er hat eine eigene kleine Truppe aus privaten Soldaten, die
ihm gleichzeitig als Personenschutz dienen.

Hycorax bietet (nach Überzeugungsarbeit) 200G + 2G für jedes Goblin-Ohr, aber
nur, wenn die Goblinplage der Gegend beseitigt wird. Falls nicht, gibt es nur 2G
für jedes rechte Ohr.

### Bartender in der Wirtschaft

Ich weiß leider nicht mehr wie er heißt, aber der eine Schankwirt war schon ein
lustiger Kerl…
