# Bestiarium

## Goblins

Nervige kleine Viecher, stinken furchtbar und nerven die Bauern rund um kleine Dörfer.

## Wölfe

Wölfe gibt es in verschiedenen Größen und Ausprägungen, wir hatten zum Beispiel einen Konflikt mit ein paar Wölfen.
Hierbei war die Anzahl eher gering, aber die Größe und der Kampfeswille dieser Biester war nicht zu unterschätzen.