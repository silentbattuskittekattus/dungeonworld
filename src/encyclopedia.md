# Enzyklopädie

## [Aidan](./aidan.md)
mein Hauptcharakter

## [Albion](./world.md#albion)
die Spielwelt in der wir uns befinden

## [Alix](./player_characters.md#alix)
Barbarin

## [Elana](./player_characters.md#elana)
Druidin

## [Goblins](./beastiary.md#goblins)
feindsinnige Monster

## [Hycorax Kornwald](./npcs.md#hycorax-kornwald)
der Dorfälteste von [Kornwald](./encyclopedia.md#kornwald)

## [Kornwald](./world.md#dorf-kornwald)
ein kleines Dorf, Umschlagplatz für Händler

## [Wölfe](./beastiary.md#wölfe)
feindsinnige Monster
