# Spieler Charaktere

## Aidan
[Mein Charakter](./aidan.md), ein junger Magier, der noch herausfinden muss, was
genau in seiner Vergangenheit passiert ist.

## Alix
Alix ist eine Barbarin, wie man sie sich vorstellt: sie trägt nur Fetzen am Leib
und interessiert sich nicht für zwischenmenschliche Zeitverschwendung. Sie will
hauptsächlich eins: kämpfen. Ansonsten kann man sie noch für Pferde
interessieren, es macht ihr scheinbar viel Spaß diese Tiere zu zähmen und als
Reittiere zu benutzen. Alix ist ähnlich wie Elana auch ca. 30 Jahre alt.

## Elana
Kennt Aidan von früher, Vertraute seiner Eltern und teilweise Lehrerin. Elana
ist eine Druidin(?) mit einem sechseckigen Saphir auf der Brust, was eine Art
Tribal ihres Volkes darstellt. Elana ist ca. 30 Jahre alt und ein guter Mensch.
